# Community Support

This project contains issues and materials in support of GitLab Open Source Partners as they migrate to and initiate new projects on GitLab.

This project is **public** and acts as the default support channel for open source partners.

To open a **private** support request with the GitLab Open Source Partners service desk, please email `ospartners@gitlab.com`.
