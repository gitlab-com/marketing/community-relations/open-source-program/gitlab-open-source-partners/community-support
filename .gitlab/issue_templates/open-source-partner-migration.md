<!-- Please title your issue the following way: "{Project Name} migration to GitLab" -->

## Migration summary

<!-- 
Briefly explain what your project is migrating to GitLab and why it has chosen to do so. Please include announcements your community has made about its decision, if any.
-->

## Migration goals

<!-- 
List your project's goals in migrating to GitLab. Delete this section if you don't want to enumerate goals. 
-->

## Migration timeline

<!-- 
If unsure, simply leave this part blank and we'll complete it later. 
-->

* Anticipated migration commencement: 
* Anticiapted migration conclusion: 

## Platform

<!-- Choose from the following options -->

* GitLab Ultimate (SaaS)
* GitLab Ultimate (Self-managed)
* GitLab Community Edition

## Current tooling and planned replacements

<!-- 
Complete the table below to outline the specific tooling involved in your migration. In the left column, list your current tooling and the function it performs for your project. In the right column, list the GitLab feature you hope will replace this tooling. 
-->

| Current tool and function | Replacement GitLab feature | 
| --- | --- |
| {e.g., Gitolite for Git mirroring} | {e.g., repository mirroring} |
| {e.g., Phabricator for continuous integration} | {e.g. pipelines} |

## Collaborators

<!-- 
Please list community members who will be participating in and assisting with the migration. Be sure to include their GitLab handles if applicable. 
-->

* 
* 
* 

## Related issues

<!-- 
Link pre-existing, open issues related to your migration. Add the title of each issue and link to it (preferably with an embedded link). You'll most likely continue editing this section as the migration progresses, so don't worry if it's mostly blank for now. 
-->

### Blocking

* [ ] [issue-title](issue-link)
* [ ] [issue-title](issue-link)
* [ ] [issue-title](issue-link)

### Important, non-blocking

* [ ] [issue-title](issue-link)
* [ ] [issue-title](issue-link)
* [ ] [issue-title](issue-link)

### Nice to have

* [ ] [issue-title](issue-link)
* [ ] [issue-title](issue-link)
* [ ] [issue-title](issue-link)

/assign @s_awezec
